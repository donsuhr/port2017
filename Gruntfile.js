'use strict';

const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const path = require('path');
const loadDotEnv = require('cmc-load-dot-env');

if (
    !{}.hasOwnProperty.call(process.env, 'AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL')
) {
    loadDotEnv();
}

const modulesDir = path.resolve(require.resolve('cmc-load-dot-env'), '../..');

module.exports = function gruntfile(grunt) {
    const config = {
        app: 'app',
        dist: 'dist',
        sassOutput: '.tmp/sass-out/',
        livereloadPort: 35729,
        testLiveReload: 35730,
        modulesDir,
    };

    timeGrunt(grunt);

    loadGruntConfig(grunt, {
        configPath: path.resolve('./build/grunt'),
        data: config,
    });

    grunt.registerTask('serve', (target) => {
        if (target === 'dist') {
            grunt.task.run(['build', 'connect:dist:keepalive']);
        } else {
            grunt.task.run([
                'clean',
                'exec:write-config',
                'exec:build-js',
                'exec:metalsmith-dist',
                'exec:sass',
                'exec:postcss',
                'connect:livereload',
                'concurrent:serve',
            ]);
        }
    });

    grunt.registerTask('build', [
        'clean',
        'exec:write-config',
        'concurrent:dist',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'exec:critical-css',
        'exec:postcss-min',
        'filerev',
        'usemin',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('styles', [
        'exec:sass',
        'exec:critical-css',
        'exec:postcss-min',
        'copy:dist',
        'filerev',
        'notify:styles',
    ]);

    grunt.registerTask('pages', [
        'exec:write-config',
        'exec:write-sitemap',
        'exec:metalsmith-dist',
        'usemin',
        'exec:inline',
        'htmlmin',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);

    grunt.registerTask('netlify', ['build', 'build-search-index']);

    grunt.registerTask('build-search-index', [
        'aws_s3:search-index-json--get',
        'exec:build-search-index',
        'aws_s3:search-index-json--put',
    ]);
};
