'use strict';

const webpack = require('webpack');
const path = require('path');
const AssetsPlugin = require('assets-webpack-plugin');
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');

const production = process.env.NODE_ENV === 'production';
const mode = production ? 'production' : 'development';

const entry = {
    'cmc-site': [path.join(__dirname, 'app/scripts/index.js')],
    'page--search-results': [
        path.join(__dirname, 'app/scripts/pages/search-results'),
    ],
};

if (!production) {
    entry['cmc-site'].unshift(
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
    );
}

const plugins = [
    new SubresourceIntegrityPlugin({
        hashFuncNames: ['sha384'],
        enabled: mode === 'production',
    }),
    new AssetsPlugin({
        entrypoints: false,
        prettyPrint: true,
        filename: 'webpack-assets.json',
        integrity: true,
    }),
    new AssetsPlugin({
        entrypoints: true,
        prettyPrint: true,
        filename: 'webpack-manifest.json',
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        },
    }),

    new webpack.IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/,
    }), // Ignore all optional deps of moment.js
];

if (!production) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = {
    devtool: production ? 'source-map' : 'eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),
    entry,
    mode,
    plugins,
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            maxInitialRequests: 25,
            maxAsyncRequests: Infinity,
            minSize: 20000,
            chunks: 'all',
        },
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
        },
        extensions: ['.js', '.jsx'],
    },
    output: {
        crossOriginLoading: 'anonymous',
        path: path.join(__dirname, 'dist/'),
        filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        chunkFilename: production
            ? 'scripts/[name]-[chunkhash].js'
            : 'scripts/[name].js',
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                include: [
                    path.join(__dirname, '/app/'),
                    path.join(__dirname, '/config.js'),
                ],
            },
            {
                test: /\.hbs$/,
                use: [
                    {
                        loader: 'handlebars-loader',
                        options: {
                            debug: false,
                            runtime: 'handlebars/runtime',
                            helperDirs: [`${__dirname}/app/metalsmith/helpers`],
                            partialDirs: [
                                `${__dirname}/app/metalsmith/partials`,
                            ],
                        },
                    },
                ],
                include: [path.join(__dirname, '/app/')],
            },
        ],
    },
};
