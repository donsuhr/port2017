import algoliasearch from 'algoliasearch/lite';

export default function createAlgoliaSearchClient(appId, key) {
    return algoliasearch(appId, key);
}
