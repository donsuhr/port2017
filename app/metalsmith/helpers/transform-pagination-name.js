'use strict';

module.exports = function transformPaginationName(name, options) {
    const matches = name.match(/(\d{4})/i);
    const year = matches[1];
    return `${year}`;
};
