'use strict';

const links = {
    SyntaxHighlighter: 'http://alexgorbatchev.com/SyntaxHighlighter/',
    'ua-parser-js': 'http://faisalman.github.io/ua-parser-js/',
    'source-map': 'https://github.com/mozilla/source-map',
    'jquery.appear': 'https://github.com/morr/jquery.appear',
    Assemble: 'http://assemble.io/',
    Mailgun: 'https://www.mailgun.com/',
    Auth0: 'https://auth0.com/',
    'aws-sdk': 'https://aws.amazon.com/sdk-for-node-js/',
    'jquery.mobile-1.0a3': 'https://jquerymobile.com/',
    'react-bootstrap-table':
        'http://allenfang.github.io/react-bootstrap-table/',
    recharts: 'http://recharts.org/',
    'JW Player': 'http://www.longtailvideo.com/players',
    'jQuery Tools': 'http://flowplayer.org/tools/',
    jCarousel: 'http://sorgalla.com/jcarousel/',
    'jQuery BBQ': 'http://benalman.com/projects/jquery-bbq-plugin/',
    'JavaScript implementation of MD5': 'http://pajhome.org.uk/crypt/md5',
    Ant: 'https://ant.apache.org',
    'Google Closure Tools': 'http://code.google.com/closure/',
    jsTestDriver: 'http://code.google.com/p/js-test-driver/',
    'Chart.js': 'http://www.chartjs.org/',
    'bootstrap-daterangepicker': 'http://www.daterangepicker.com/',
    SPServices: 'https://spservices.codeplex.com/',
    'i18n.js': 'http://requirejs.org/docs/api.html#i18n',
    'node-jsonwebtoken': 'https://github.com/auth0/node-jsonwebtoken',
    'webpack-amdi18n-loader':
        'https://github.com/futuweb/webpack-amdi18n-loader',
    'Kendo UI': 'http://www.telerik.com/kendo-ui',
    LessHat: 'http://lesshat.madebysource.com/',
    'jQuery-File-Upload': 'https://github.com/blueimp/jQuery-File-Upload',
    Susy: 'https://susy.oddbird.net/',
    Breakpoint: 'http://breakpoint-sass.com/',
    'jquery-seat-charts':
        'https://github.com/mateuszmarkowski/jQuery-Seat-Charts',
    'SharePoint JavaScript Object Model (JSOM)':
        'http://msdn.microsoft.com/en-us/library/hh185006(v=office.14).aspx',
    Bluebird: 'http://bluebirdjs.com/',
    'GreenSock GSAP': 'https://greensock.com/gsap/',
    'GreenSock ScrollTrigger': 'https://greensock.com/scrolltrigger/',
    LottieFiles: 'https://lottiefiles.com/',
    Handlebars: 'https://handlebarsjs.com/',
    Plasmic: 'https://www.plasmic.app/',
};

module.exports = function stackLinkLookup(name, opts) {
    if (Object.hasOwnProperty.call(links, name)) {
        return `<a href="${links[name]}" target="_blank" rel="noreferrer noopener">${name}</a>`;
    }
    return name;
};
