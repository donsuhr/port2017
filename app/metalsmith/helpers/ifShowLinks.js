'use strict';

module.exports = function ifShowLinks(opts) {
    const isListPage = this.fromList && !this['hide-project-link'];
    if (
        isListPage
        || this['project-link']
        || this['repo-link']
        || this['other-links']
    ) {
        return opts.fn(this);
    }
    return opts.inverse(this);
};
