'use strict';

const Handlebars = require('handlebars');

module.exports = function getPagination(key, partial, options) {
    const { pages } = options.data.root.paginationUse[key];
    const hbPartial = Handlebars.partials[partial];
    const template = typeof hbPartial === 'string'
        ? Handlebars.compile(hbPartial)
        : hbPartial;
    return new Handlebars.SafeString(
        template({
            items: pages,
            currentPath: options.data.root.path,
        }),
    );
};
