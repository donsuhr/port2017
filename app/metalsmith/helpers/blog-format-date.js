'use strict';

const moment = require('moment');

module.exports = function blogFormatDate(date, options) {
    return moment(date).format('MMMM Do, YYYY');
};
