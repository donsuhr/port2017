'use strict';

const moment = require('moment');

module.exports = function transformPaginationNameArchive(pagination, options) {
    const date = moment(pagination.name, 'YYYY', true);
    return `${date.format('YYYY')}`;
};
