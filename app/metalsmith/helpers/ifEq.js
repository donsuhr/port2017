'use strict';

// http://stackoverflow.com/questions/15088215/handlebars-js-if-block-helper
module.exports = function ifEq(a, b, opts) {
    if (a === b) {
        return opts.fn(this);
    }
    return opts.inverse(this);
};
