'use strict';

const moment = require('moment');
const isDate = require('lodash/isDate');

const addPubDateFormatted = function addPubDateFormatted(config) {
    return (files, metalsmith, done) => {
        Object.keys(files).forEach((filename) => {
            const file = files[filename];
            const hasPubDate = file.publish && isDate(file.publish);
            const hasStatDate = file.stats && file.stats.birthtime;
            if (hasPubDate || hasStatDate) {
                const fileDate = hasPubDate
                    ? moment(file.publish)
                    : moment(file.stats.birthtime);
                file.publishFormatted = fileDate.format('M/D/YYYY');
            }
        });
        done();
    };
};

module.exports = addPubDateFormatted;
