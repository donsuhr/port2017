'use strict';

const fs = require('fs');
const path = require('path');

const webpackAssets = function webpackAssets(config) {
    return (files, metalsmith, done) => {
        const hrstart = process.hrtime();
        const manifestFs = fs.readFileSync(
            path.resolve(process.cwd(), 'webpack-manifest.json'),
            'utf8',
        );
        const assetsFs = fs.readFileSync(
            path.resolve(process.cwd(), 'webpack-assets.json'),
            'utf8',
        );

        const manifestData = JSON.parse(manifestFs);
        const assetsData = JSON.parse(assetsFs);

        Object.keys(files).forEach((filename) => {
            const file = files[filename];
            const additonalScripts = file['additional-scripts'] || [];
            const pageScripts = [...[].concat(manifestData['cmc-site'].js)];

            additonalScripts.forEach((script) => {
                try {
                    const assetsKey = path.basename(script.src, '.js');
                    if (manifestData[assetsKey]) {
                        pageScripts.push(
                            ...[].concat(manifestData[assetsKey].js),
                        );
                    } else {
                        // eslint-disable-next-line no-console
                        console.error(
                            'error finding webpack asset for script',
                            script,
                            'in',
                            filename,
                        );
                    }
                } catch (e) {
                    // eslint-disable-next-line no-console
                    console.error(
                        'error linking to script',
                        script,
                        'in',
                        filename,
                    );
                }
            });

            const result = Array.from(new Set(pageScripts)).map((src) => {
                const [, { jsIntegrity = null }] = Object.entries(
                    assetsData,
                ).find(
                    ([, { js }]) => (Array.isArray(js) && js.includes(src)) || js === src,
                );

                return {
                    src,
                    jsIntegrity,
                };
            });
            file.scripts = result;
        });
        const hrend = process.hrtime(hrstart);
        // eslint-disable-next-line no-console
        console.info(
            'Metalsmith - webpackAssets \t\tExecution time: %ds %dms',
            hrend[0],
            hrend[1] / 1000000,
        );
        setImmediate(done);
    };
};

module.exports = webpackAssets;
