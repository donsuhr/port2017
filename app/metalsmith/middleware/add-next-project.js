/* eslint no-underscore-dangle: ["error", { "allow": ["_metadata"] }] */

'use strict';

const addNextProject = function addNextProject(config) {
    return (files, metalsmith, done) => {
        Object.keys(files).forEach((filename) => {
            const file = files[filename];
            const { blog } = metalsmith._metadata.collections;
            const index = blog.findIndex((x) => x.path === file.path);

            let olderIndex = index + 1;
            let olderFound = false;
            while (olderIndex < blog.length && !olderFound) {
                if (blog[olderIndex].publish) {
                    olderFound = true;
                    file.olderProjectPath = blog[olderIndex].path;
                }
                olderIndex += 1;
            }

            let newerIndex = index - 1;
            let newerFound = false;
            while (newerIndex >= 0 && !newerFound) {
                if (blog[newerIndex].publish) {
                    newerFound = true;
                    file.newerProjectPath = blog[newerIndex].path;
                }
                newerIndex -= 1;
            }
        });
        done();
    };
};

module.exports = addNextProject;
