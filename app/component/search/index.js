/* eslint no-underscore-dangle: ["error", {"allow": ["_highlightResult"]}] */

import config from 'config';
import queryString from 'query-string';
import moment from 'moment';
import sanitizeHtml from 'sanitize-html';

import createAlgoliaSearchClient from '../../scripts/algoliaSearchClient';
import template from './search-results.template.hbs';
import snippetFn from './snippet';
// import results from './results6.json';

const searchClient = createAlgoliaSearchClient(
    config.algolia.appId,
    config.algolia.searchKey,
);
const searchIndex = searchClient.initIndex('port2017');

const parsed = queryString.parse(window.location.search);
const query = parsed.q;

function resultMapper(x) {
    const title = sanitizeHtml(x._highlightResult.title.value, {
        allowedTags: ['em'],
    });
    const snippet = x._highlightResult.snippet
        ? x._highlightResult.snippet.value
        : false;

    const cleanBody = sanitizeHtml(x._highlightResult.body.value, {
        allowedTags: ['em'],
    });
    const body = snippet
        || snippetFn(
            'em',
            cleanBody
                .replace(/(?:\n|\r)+/g, '')
                .replace(/\s\s+/g, ' ')
                .trim(),
            x._highlightResult.body.matchedWords,
            20,
        );
    const momentPublish = moment.unix(x.publish);
    const publish = momentPublish.isValid() ? momentPublish : null;
    return {
        title,
        body,
        url: x.url,
        category: x.category,
        type: x.type,
        publish,
    };
}

const target = document.querySelector('#SearchResultContainer');
searchIndex
    .search(query, {
        attributesToRetrieve: ['category', 'url', 'publish', 'type'],
        attributesToSnippet: [],
        attributesToHighlight: ['body', 'title', 'snippet'],
    })
    .then((content) => {
        if (content.nbHits > 0) {
            const vm = content.hits.map(resultMapper);
            target.innerHTML = template({
                data: vm,
            });
        } else {
            target.innerHTML = `<p>No items matched the query '${query}'`;
        }
    })
    .catch((err) => {
        target.innerHTML = 'Error loading search results';
    });

// const vm = results.hits.map(resultMapper);
//
// document.querySelector('#SearchResultContainer').innerHTML = template({
//     data: vm,
// });
