import sortBy from 'lodash/sortBy';

function truncate({
    text, stop, start = 0, margin = 0,
}) {
    const words = text.split(' ');
    const wStart = Math.max(0, start - (margin + 1));
    const wEnd = Math.min(words.length, stop + margin + 2);
    const chunk = words.slice(wStart, wEnd);
    let ret = chunk.join(' ').trim();
    if (wStart > 0) {
        ret = `...${ret}`;
    }
    if (wEnd < words.length) {
        ret += '...';
    }
    return ret;
}

/* eslint-disable-next-line default-param-last */
export default function snippet(hlTag = 'em', text, matched, numWords = 30) {
    const matchedOrs = matched.join('|');
    if (matchedOrs.length === 0) {
        return truncate({ text, stop: numWords });
    }

    const regex = new RegExp(
        // eslint-disable-next-line no-useless-escape
        `<${hlTag}>\\s*(?:${matchedOrs})\\s*<\/${hlTag}>`,
        'gim',
    );

    const matches = [];
    let currentMatch = regex.exec(text);
    while (currentMatch) {
        matches.push(currentMatch);
        currentMatch = regex.exec(text);
    }
    if (matches.length === 0) {
        return truncate({ text, stop: numWords });
    }

    matches.forEach((x, i) => {
        x.groupCaptured = false;
        if (x.index === 0) {
            x.wordIndex = 0;
        } else {
            x.wordIndex = text.substring(0, x.index).match(/\S+/g).length;
        }
        x.numWordsToEnd = text.substring(x.index).match(/\S+/g).length;
        if (i > 0) {
            x.numWordsToPrevious = x.wordIndex - matches[i - 1].wordIndex;
            if (i < matches.length) {
                matches[i - 1].numWordsToNext = x.numWordsToPrevious;
            }
        }
    });

    const groups = [];
    matches.forEach((x, i) => {
        let count = 0;
        if (!x.groupCaptured) {
            do {
                const currentItem = matches[i + count];
                const val = {
                    wordIndex: x.wordIndex,
                    length: currentItem.wordIndex - x.wordIndex,
                    count: count + 1,
                };
                if (count > 0) {
                    // replace the last item for this match
                    groups.splice(groups.length - 1, 1, val);
                } else {
                    groups.push(val);
                }

                currentItem.groupCaptured = true;

                count += 1;
            } while (
                i + count < matches.length
                && matches[i + count].wordIndex - x.wordIndex < numWords
            );
        }
    });
    const longest = sortBy(groups, '- length', 'wordIndex');
    const longestIndex = groups.findIndex(
        (x) => x.wordIndex === longest[0].wordIndex,
    );
    const groupWordTotal = groups.reduce((acc, x) => acc + x.length, 0);
    const margin = Math.max(4, (numWords - groupWordTotal) / groups.length);
    groups.forEach((x) => {
        x.snippet = truncate({
            text,
            stop: x.wordIndex + x.length,
            start: x.wordIndex,
            margin,
        });
    });

    let ret = '';
    let index = longestIndex;
    while (index >= 0 && ret.split(' ').length < numWords) {
        ret = `${groups[index].snippet}${ret}`;
        index -= 1;
    }
    index = longestIndex + 1;
    while (index < groups.length && ret.split(' ').length < numWords) {
        ret += groups[index].snippet;
        index += 1;
    }
    return ret.replace(/\.\.\.\.\.\./g, '... ');
}
