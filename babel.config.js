'use strict';

module.exports = function config(api) {
    api.cache(true);

    const presets = [
        [
            '@babel/preset-env',
            {
                modules: false,
                loose: true,
                useBuiltIns: 'usage',
                corejs: { version: '3.27', proposals: true },
                debug: false,
            },
        ],
    ];
    const plugins = [];

    return {
        presets,
        plugins,
    };
};
