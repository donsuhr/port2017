import { generate } from "critical";
import process from "node:process";
import path from "node:path";
import fs from "node:fs";

// https://stackoverflow.com/questions/13542667/create-directory-when-writing-to-file-in-node-js
function ensureDirectoryExistence(filePath) {
    const dirname = path.dirname(filePath);
    if (!fs.existsSync(dirname)) {
        ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
    }
}

function loadForceFile(src) {
    return new Promise((resolve, reject) => {
        const file = path.resolve(src);
        fs.readFile(file, "utf8", (rfErr, data) => {
            if (rfErr) {
                return reject(rfErr);
            }
            return resolve(data);
        });
    });
}

function work({ src, name }) {
    return generate({
        base: "./dist/",
        src,
        css: ["./.tmp/sass-out/main.css"],
        // dest: `../.tmp/styles/critical-${name}.css`,
        inline: false,
        extract: false,
        ignore: {
            atrule: ["@font-face"],
            rule: [/modal/],
        },
        dimensions: [
            {
                width: 414,
                height: 896,
            },
            {
                width: 1280,
                height: 960,
            },
        ],
        penthouse: {
            screenshots: {
                basePath: `./.tmp/crit-out/${name}`,
            },
        },
    }).then((result) => ({
        src,
        name,
        result,
    }));
}

const items = [
    { src: "index.html", name: "home" },
    { src: "blog/nest/index.html", name: "nest" },
    // { src: 'products/radius-by-campus-management/index.html', name: 'radius' },
];

loadForceFile("./.tmp/sass-out/crit-forced.css").then((forceCss) => {
    ensureDirectoryExistence(
        path.resolve(process.cwd(), "./.tmp/crit-out/foo")
    );
    Promise.all(items.map(work)).then((results) => {
        const combinedResults = `${forceCss} \n${results
            .map((x) => x.result.css)
            .join("\n")}`;
        const combinedOutputFile = path.resolve(
            process.cwd(),
            "./.tmp/sass-out/critical.css"
        );
        fs.writeFileSync(combinedOutputFile, combinedResults, "utf8");

        ensureDirectoryExistence(
            path.resolve(process.cwd(), "./.tmp/crit-out")
        );

        results.forEach(({ src, name, result }) => {
            const outputFile = path.resolve(
                process.cwd(),
                `./.tmp/crit-out/critical-${name}.css`
            );
            ensureDirectoryExistence(outputFile);
            fs.writeFileSync(outputFile, result.css, "utf8");
        });
    });
});
