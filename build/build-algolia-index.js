'use strict';

const { argv } = require('yargs');
const loadDotEnv = require('cmc-load-dot-env');
const algoliasearch = require('algoliasearch');

const { buildSearchIndex } = require('build-algolia-index');

if (!{}.hasOwnProperty.call(process.env, 'CORS_ORIGIN_TLD')) {
    // eslint-disable-next-line no-console
    console.log('no CORS_ORIGIN_TLD set, will load .env');
    loadDotEnv();
}

const searchClient = algoliasearch(
    process.env.ALGOLIA_APPID,
    process.env.ALGOLIA_ADMIN_KEY,
);

const dryRun = !!argv['dry-run'];

const groups = [
    {
        json: 'search-index--port2017.json',
        algoliaIndex: 'port2017',
        src: ['app/pages/**/*.html'],
    },
];

buildSearchIndex(
    groups,
    searchClient,
    dryRun,
    process.env.CORS_ORIGIN_CMC,
    process.env.CORS_ORIGIN_API,
);
