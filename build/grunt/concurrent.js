'use strict';

module.exports = function concurent(grunt, options) {
    return {
        options: {
            logConcurrentOutput: true,
            limit: 6,
        },
        dist: ['exec:sass', 'exec:build-js--quiet', 'copy:dist'],
        serve: [
            // 'watch:sass',
            // 'watch:js',
            // 'watch:livereload',
            'exec:watch-linters',
            'exec:metalsmith-watch',
            'exec:sass-watch',
            'exec:postcss-watch',
        ],
    };
};
