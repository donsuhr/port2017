'use strict';

const { argv } = require('yargs');

const secureFlag = argv.secure ? '--secure' : '';

module.exports = function run(grunt, options) {
    return {
        'build-js': {
            cmd: 'npm run build:js',
            stdio: 'inherit',
        },
        'build-js--quiet': {
            cmd: 'npm run build:js:quiet',
            stdio: 'inherit',
        },
        'build-search-index': {
            cmd: 'npm run build:search',
            stdio: 'inherit',
        },
        'critical-css': {
            cmd: 'node ./build/critical-css.mjs',
            stdio: 'inherit',
        },
        inline: {
            cmd: 'npm run inline',
            stdio: 'inherit',
        },
        lint: {
            cmd: 'npm run lint',
            stdio: 'inherit',
        },
        'lint-sass': {
            cmd: 'npm run lint:styles',
            stdio: 'inherit',
        },
        'metalsmith-dist': {
            cmd: 'npm run build:html',
            stdio: 'inherit',
        },
        'metalsmith-watch': {
            cmd: 'npm run watch:html',
            stdio: 'inherit',
        },
        postcss: {
            cmd: 'npm run postcssprefix -- --dir .tmp/styles',
            stdio: 'inherit',
        },
        'postcss-watch': {
            cmd: 'npm run postcssprefix -- --dir .tmp/styles --watch',
            stdio: 'inherit',
        },
        'postcss-min': {
            cmd: 'npm run postcssprefix -- --dir dist/styles --minify',
            stdio: 'inherit',
        },
        sass: {
            cmd: 'npm run sass',
            stdio: 'inherit',
        },
        'sass-watch': {
            cmd: 'npm run sass -- --watch',
            stdio: 'inherit',
        },
        'watch-linters': {
            cmd: `node ./build/watch.js --color ${secureFlag}`,
            stdio: 'inherit',
        },
        'write-config': {
            cmd: 'npm run build:write-config',
            stdio: 'inherit',
        },
        'write-sitemap': {
            cmd: 'npm run build:write-sitemap',
            stdio: 'inherit',
        },
    };
};
