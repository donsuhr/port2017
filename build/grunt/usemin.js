'use strict';

module.exports = function useminCof(grunt, options) {
    return {
        options: {
            assetsDirs: [options.dist],
        },
        html: {
            src: `${options.dist}/**/*.html`,
        },
    };
};
