'use strict';

module.exports = function fileref(grunt, options) {
    return {
        options: {
            algorithm: 'md5',
            length: 8,
        },
        css: {
            src: `${options.dist}/**/*.css`,
        },
    };
};
