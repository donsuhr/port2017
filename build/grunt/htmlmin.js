'use strict';

module.exports = function htmlmin(grunt, options) {
    return {
        dist: {
            options: {
                collapseBooleanAttributes: true,
                collapseWhitespace: true,
                conservativeCollapse: true,
                removeAttributeQuotes: true,
                removeCommentsFromCDATA: true,
                removeEmptyAttributes: true,
                removeOptionalTags: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                caseSensitive: true,
            },
            files: [
                {
                    expand: true,
                    cwd: options.dist,
                    src: ['**/*.html', '!sso-iframe.html'],
                    dest: options.dist,
                },
            ],
        },
    };
};
