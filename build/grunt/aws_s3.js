'use strict';

const { oneLineTrim } = require('common-tags');

module.exports = function awsS3(grunt, options) {
    return {
        options: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL,
            secretAccessKey:
                process.env.AWS_SECRET_ACCESS_KEY__CMC_DEMOCENTER_DL,
            region: process.env.AWS_REGION,
            uploadConcurrency: 5,
            downloadConcurrency: 5,
            progress: 'none',
        },
        'dl-asset-overlay': {
            options: {
                bucket: oneLineTrim`${process.env.AWS_S3_BUCKET_PREFIX}file-asset-overlay--
                        ${process.env.AWS_S3_FILE_OVERLAY_SUFFIX}`,
                differential: true,
            },
            files: [
                { dest: '/', cwd: 'file-asset-overlay/', action: 'download' },
            ],
        },
        'search-index-json--get': {
            options: {
                bucket: oneLineTrim`${process.env.AWS_S3_BUCKET_PREFIX}search-index--
                        ${process.env.AWS_S3_FILE_OVERLAY_SUFFIX}`,
                differential: true,
            },
            files: [{ dest: '/', cwd: 'search-index/', action: 'download' }],
        },
        'search-index-json--put': {
            options: {
                bucket: oneLineTrim`${process.env.AWS_S3_BUCKET_PREFIX}search-index--
                        ${process.env.AWS_S3_FILE_OVERLAY_SUFFIX}`,
                differential: true,
            },
            files: [
                {
                    dest: '/',
                    cwd: 'search-index/',
                    expand: true,
                    src: ['*.json'],
                    action: 'upload',
                },
            ],
        },
    };
};
