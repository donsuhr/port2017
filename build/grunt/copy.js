'use strict';

module.exports = function copyConfig(grunt, options) {
    return {
        dist: {
            files: [
                {
                    // touch icons, webp imgs, robots
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: [
                        '*.{ico,png,txt}',
                        'images/{,*/}*.webp',
                        '_redirects',
                    ],
                },
                {
                    // unprocessed css
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/css/`,
                    src: ['**/*.*'],
                    dest: `${options.dist}/styles`,
                },
                {
                    // google webmaster tools
                    expand: true,
                    dot: true,
                    cwd: `${options.app}`,
                    src: ['google*.html'],
                    dest: `${options.dist}`,
                },
                {
                    // netlify not doing imagemin, it does it on its own
                    expand: true,
                    dot: true,
                    cwd: `${options.app}/images/`,
                    src: ['**/*.*'],
                    dest: `${options.dist}/images`,
                },
            ],
        },
        overlay: {
            files: [
                {
                    // netlify not doing imagemin, it does it on its own
                    expand: true,
                    dot: true,
                    cwd: 'file-asset-overlay',
                    src: ['**/*.*'],
                    dest: options.dist,
                },
            ],
        },
    };
};
