'use strict';

const dotEnv = require('dotenv');
const path = require('path');
const fs = require('fs');
const { argv } = require('yargs');
const merge = require('lodash/merge');
const isEqual = require('lodash/isEqual');
const assignIn = require('lodash/assignIn');
const moment = require('moment');

dotEnv.config();

const Metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const inPlace = require('metalsmith-in-place');
const helpers = require('metalsmith-register-helpers');
const publish = require('metalsmith-publish');
const chokidar = argv.watch ? require('chokidar') : null;
const collections = require('@metalsmith/collections');
const pagination = require('metalsmith-pagination');
const defaultValues = require('@metalsmith/default-values');
const permalinks = require('@metalsmith/permalinks');
const metalsmithEnv = require('metalsmith-env');
const sitemap = require('metalsmith-sitemap');
const handlebars = require('handlebars');
const handlebarsLayouts = require('handlebars-layouts');
const categoryPagination = require('metalsmith-category-pagination');
const webpackAssets = require('../app/metalsmith/middleware/metalsmith-webpack-assets');
const addNext = require('../app/metalsmith/middleware/add-next-project');

// assemble helpers have the handlebars-layouts in it. assemble first

require('handlebars-helpers')({
    handlebars,
}); // http://assemble.io/helpers/

// overwrite second
handlebars.registerHelper(handlebarsLayouts(handlebars));

require('@babel/register')({
    only: [/config.js/],
    presets: ['@babel/preset-env'],
});

const config = require('../config').default;

const production = process.env.NODE_ENV === 'production';

const publishConfig = {
    draft: !production,
    private: !production,
    future: !production,
};

const projectListPaginationMetadata = {
    collection: ['blog'],
    'menu-priority': 0,
    title: 'Projects',
    private: true,
};

const webpackAssetsFilePath = 'webpack-manifest.json';
let webpackManifestJson = JSON.parse(
    fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
);

function compareWebpackManifest() {
    const newJson = JSON.parse(
        fs.readFileSync(path.resolve(process.cwd(), webpackAssetsFilePath)),
    );
    const result = isEqual(webpackManifestJson, newJson);
    webpackManifestJson = newJson;
    return result;
}
function build(clean, collectionsObj, changedFile) {
    const t1 = process.hrtime();
    const ms = new Metalsmith(path.resolve(`${__dirname}/../`));
    console.log('Metalsmith build start'); // eslint-disable-line no-console

    const projectListPaginationLayout = fs.readFileSync(
        path.resolve(
            `${__dirname}/../app/metalsmith/partials/layouts/projectListPage.hbs`,
        ),
    );

    ms.clean(!!clean)
        .source('app/pages')
        .destination('dist')
        .metadata({
            config,
        })
        .ignore((currentPath, lstat) => {
            // true to ignore
            if (changedFile) {
                if (lstat.isDirectory()) {
                    // don't ignore directories, need to search sub dirs
                    return false;
                }
                let parent = changedFile
                    .split(path.sep)
                    .slice(0, -1)
                    .join(path.sep);
                while (parent.length) {
                    if (
                        currentPath.indexOf(
                            `${parent}${path.sep}index.html`,
                        ) !== -1
                    ) {
                        // is parent index file, dont ignore
                        return false;
                    }
                    parent = parent.split(path.sep).slice(0, -1).join(path.sep);
                }
                // check for same directory sibling files and below
                return currentPath.indexOf(changedFile) === -1;
            }
            // no file changed, ignore nothing
            return false;
        })
        .use(metalsmithEnv())
        .use(
            defaultValues([
                {
                    pattern: '**/*.html',
                    defaults: {
                        layout: 'main.hbs',
                        'url-lang-prefix': '',
                        'menu-priority': 0.5,
                        'show-in-quick-launch': true,
                        'show-in-left-nav': true,
                        'og-type': 'article',
                        leftNavStartFrom: 'pages',
                    },
                },
            ]),
        )
        .use(publish(publishConfig))
        .use(collections(collectionsObj))

        .use(
            // add main paging, ie page-1.html, page-2.html
            pagination({
                'collections.blog': {
                    layout: 'main.hbs',
                    first: 'index.html',
                    path: 'blog/page-:num.html',
                    pageContents: projectListPaginationLayout,
                    filter: (file) => !/index.html/.test(file.path),
                    pageMetadata: projectListPaginationMetadata,
                },
            }),
        )

        .use(
            // add monthly paging, ie 2000-December.html 2005-February.html
            pagination({
                'collections.blog': {
                    layout: 'main.hbs',
                    path: 'blog/:name.html',
                    pageContents: projectListPaginationLayout,
                    key: 'yearly',
                    filter: (file) => !/index.html/.test(file.path),
                    groupBy: (file, index, options) => moment(file.publish).format('YYYY'),
                    pageMetadata: assignIn(
                        {},
                        {
                            'is-archive-page': true,
                        },
                        projectListPaginationMetadata,
                    ),
                },
            }),
        )

        .use(
            // add category pages, ie javascript.html, vue.html, sharepoint.html
            categoryPagination({
                collection: 'collections.blog',
                pageContents: projectListPaginationLayout,
                addPageMetadata: assignIn(
                    {},
                    {
                        'is-category-page': true,
                    },
                    projectListPaginationMetadata,
                ),
                layout: 'main.hbs',
            }),
        )
        .use(webpackAssets({}))
        .use(
            permalinks({
                relative: false,
            }),
        )
        .use(addNext())
        .use(
            sitemap({
                hostname: 'https://www.donsuhr.com',
                omitIndex: true,
                omitExtension: true,
                output: 'pages.xml',
            }),
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: 'app/metalsmith/helpers',
            }),
        )
        .use(
            inPlace({
                engine: 'handlebars',
                partials: 'app/metalsmith/partials',
            }),
        )
        .use(
            layouts({
                engine: 'handlebars',
                directory: 'app/metalsmith/container',
                partials: 'app/metalsmith/partials',
            }),
        )
        .build((err, files) => {
            if (err) {
                console.log(err); // eslint-disable-line no-console
                console.log('Error rendering files. Stack:'); // eslint-disable-line no-console
                console.log(err.stack); // eslint-disable-line no-console
            } else {
                const hrend = process.hrtime(t1);
                // eslint-disable-next-line no-console
                console.info(
                    `Metalsmith - Done ${
                        Object.keys(files).length
                    } files written \tExecution time: %ds %dms`,
                    hrend[0],
                    hrend[1] / 1000000,
                );
            }
        });
}

function getdirSync(dirPath, parentName, result) {
    const stat = fs.lstatSync(dirPath);
    result = result || {};
    if (stat.isDirectory()) {
        const name = parentName
            ? `${parentName}==>${path.basename(dirPath)}`
            : path.basename(dirPath);
        const localName = name.replace(/^pages==>/, '');
        if (parentName) {
            // ignore root dir
            result[localName] = {
                path: dirPath,
                name: localName,
                refer: false,
                pattern: `${localName.replace(/==>/g, '/')}/*.html`,
            };
        }

        fs.readdirSync(dirPath).map((child) => getdirSync(`${dirPath}/${child}`, name, result));
    }
    return result;
}

const autoCollections = getdirSync(path.resolve(`${__dirname}/../app/pages`));
const specificCollections = {
    // add links and other 'sub-site' metadata here; page metadata in defaultValues
    blog: {
        sortBy: 'publish',
        reverse: true,
    },
};

const collectionsObj = merge({}, autoCollections, specificCollections);

if (!argv.watch_only) {
    build(!!argv.clean, collectionsObj);
}

let pendingBuild = new Set();
let buildTimeout;
const debounceBuild = (bool, collObj, limitToFolder) => {
    if (limitToFolder === false) {
        pendingBuild = 'all';
    }
    if (pendingBuild !== 'all') {
        pendingBuild.add(limitToFolder);
    }
    clearTimeout(buildTimeout);
    buildTimeout = setTimeout(() => {
        if (pendingBuild === 'all') {
            build(bool, collObj, false);
        } else {
            pendingBuild.forEach((x) => {
                build(bool, collObj, x);
            });
        }
        pendingBuild = new Set();
    }, 500);
};

if (argv.watch) {
    console.log('Metalsmith watching files...'); // eslint-disable-line no-console
    chokidar
        .watch([
            'app/pages/**/*.html',
            'app/metalsmith/container/**/*.hbs',
            'app/metalsmith/partials/**/*.hbs',
            'app/metalsmith/helpers/**/*.js',
        ])
        .on('change', (file) => {
            const folder = path.dirname(file);
            const type = path.extname(file);
            console.log('Metalsmith Watch: change', file); // eslint-disable-line no-console
            const limitToFolder = type === '.html' ? folder.replace('app/pages/', '') : false;
            if (file === webpackAssetsFilePath) {
                if (!compareWebpackManifest()) {
                    // eslint-disable-next-line no-console
                    console.log(
                        'webpack manifest file changed -- will rebuild',
                    );
                    debounceBuild(false, collectionsObj, false);
                } else {
                    // eslint-disable-next-line no-console
                    console.log(
                        'webpack manifest file NOT changed -- skip build',
                    );
                }
            } else {
                debounceBuild(false, collectionsObj, limitToFolder);
            }
        });
}
