'use strict';

const glob = require('glob');
const { inlineSource } = require('inline-source');
const path = require('path');
const fs = require('fs');

/*
  copies the contents of js/css files with the attribute `inline` into the html files
  like crit styles in production mode
*/

const webpackAssets = require('../webpack-assets.json');

glob('./dist/**/*.html', (er, files) => {
    files.forEach((file) => {
        const htmlpath = path.resolve(file);
        fs.readFile(htmlpath, 'utf8', (rfErr, data) => {
            if (rfErr) {
                throw rfErr;
            }
            let result = data;
            Object.keys(webpackAssets).forEach((bundleName) => {
                const bundleInfo = webpackAssets[bundleName];
                const type = Object.keys(bundleInfo)[0];
                const bundleFile = bundleInfo[type];
                const regex = new RegExp(
                    `<script\\s+src=["']/scripts/${bundleName}.js["']`,
                );
                result = result.replace(regex, `<script src="${bundleFile}"`);
            });

            fs.writeFile(htmlpath, result, 'utf8', async (wfErr) => {
                if (wfErr) {
                    throw wfErr;
                }
                try {
                    const html = await inlineSource(htmlpath, {
                        compress: false,
                        ignore: ['a'],
                        rootpath: path.resolve('dist'),
                        handlers: [
                            (source, context) => {
                                if (
                                    source
                                    && source.fileContent
                                    && (source.type === 'js'
                                        || source.type === 'css')
                                ) {
                                    // eslint-disable-next-line no-console
                                    // console.log('replacing', source.match);
                                    const smPath = source.match.match(
                                        /(?:href|src)=['"]([^'"]*)/,
                                    );
                                    const smDir = path.dirname(smPath[1]);
                                    source.fileContent = source.fileContent.replace(
                                        /# sourceMappingURL=([^\s*/]*)/,
                                        `# sourceMappingURL=${smDir}/$1\n`,
                                    );
                                }
                                return Promise.resolve();
                            },
                        ],
                    });
                    fs.writeFile(htmlpath, html, (fsError) => {
                        if (fsError) {
                            throw fsError;
                        }
                    });
                } catch (err) {
                    // eslint-disable-next-line no-console
                    console.log('\ninline-source error');
                    // eslint-disable-next-line no-console
                    console.log(err);
                }
            });
        });
    });
});
