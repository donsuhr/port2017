#!/bin/bash
dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
exclude="${dir}/rsync-exclude-from.txt"
dist="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../dist" && pwd )/"
rsync -azE --verbose --progress --stats --delete --exclude-from=${exclude} -e ssh ${dist} donsuhr.com-thecatz:/home/thecatz/donsuhr.com/
